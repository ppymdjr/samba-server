#!/bin/bash

# to update this run the following shell command:-
# IMAGE=`imgadm avail -H -ouuid name=base-64-lts | tail -n 1`
# 390639d4-f146-11e7-9280-37ae5c6d53d4  base-64-lts         17.4.0      smartos  zone-dataset  2018-01-04
IMAGE='390639d4-f146-11e7-9280-37ae5c6d53d4'
SOURCE_UUID="ed350abc-9338-11e9-b6dd-6003088cb634"  # change if you fork 
ALIAS='samba-server'
TAG='admin'
IP='dhcp'

# for DHCP nothing else is needed
MORE_IP='' 

EXISTING=''

# I think I should build up the IP configuration more flexibly. I should use the new 'ips' property too

get_existing() {
    EXISTING=`vmadm lookup customer_metadata.source_uuid=$SOURCE_UUID`
}

get_existing_or_error() {
    get_existing
    if [[ -z $EXISTING ]]; then
        return 1;
    fi
}
