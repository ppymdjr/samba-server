#!/bin/bash

EXISTING=`cat /id`
rm /id

# Stage 2 installed - runs in the vm...
echo "Executing stage 2 installer"

pkgin -y up
pkgin -y install samba-3.6

groupadd -g 500 store
useradd -u 500 -g 500 -c "Store user" -s /usr/bin/false -d /store store

# mkdir /store
zfs mount zones/$EXISTING/data/root
ln -s /zones/$EXISTING/data/root /store
chown store:store /store

cat >/opt/local/etc/samba/smb.conf <<EOF
[global]
  security = share
  load printers = no
  guest account = store

; Comment out [homes] section

[store]
  path = /store
  public = yes
  only guest = yes
  writable = yes
  printable = no
EOF

svcadm enable svc:/pkgsrc/samba:nmbd
svcadm enable svc:/pkgsrc/samba:smbd
svcadm enable dns/multicast
